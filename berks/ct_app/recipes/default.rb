#
# Cookbook:: ct_app
# Recipe:: default
#
# Copyright:: 2023, The Authors, All Rights Reserved.

package 'libimlib2' do
  action :install
end

package 'libimlib2-dev' do
  action :install
end

package 'libmysqlclient-dev' do
  action :install
end