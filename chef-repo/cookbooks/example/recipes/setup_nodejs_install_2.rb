execute 'yarn0' do
  command 'curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -'
end

execute 'yarn1' do
  command 'echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list'
end

execute 'aptupdate' do
  command 'sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates'
end

execute 'aptupdate2' do
  command 'curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -'
end

execute 'nodejs' do
  command 'sudo apt install -y nodejs'
end

execute 'yarn' do
  command 'sudo apt install -y yarn'
end


